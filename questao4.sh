#!/bin/sh

echo "Informe o IP da primeira maquina: "
read ip_1

ping -q -c 1 $ip_1 2&gt;/dev/null 1&gt;/dev/null;
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
    echo "A maquina respondeu"
else
    echo "A maquina nao respondeu"
fi

echo "Informe o IP da segunda maquina: "
read ip_2

ping -q -c 1 $ip_2 2&gt;/dev/null 1&gt;/dev/null;
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
    echo "A maquina respondeu"
else
    echo "A maquina nao respondeu"
fi

echo "Informe o IP da terceira maquina: "
read ip_3

ping -q -c 1 $ip_3 2&gt;/dev/null 1&gt;/dev/null;
RETVAL=$?

if [ $RETVAL -eq 0 ]; then
    echo "A maquina respondeu"
else
    echo "A maquina nao respondeu"
fi
