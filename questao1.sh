#!/bin/sh

echo "Informe o primeiro diretorio: "
read dir1
echo "Informe o segundo diretorio: "
read dir2
echo "Informe o terceiro diretorio: "
read dir3

echo "Total de arquivos primeiro diretorio: "
find $dir1 -maxdepth 1 -type f | wc -l

echo "Total de arquivos segundo diretorio: "
find $dir2 -maxdepth 1 -type f | wc -l

echo "Total de arquivos terceiro diretorio: "
find $dir3 -maxdepth 1 -type f | wc -l
